val serializationVersion = "1.0.0-RC"
val ktorVersion = "1.4.0"

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("plugin.serialization") version "1.3.61"
    id("kotlin-android-extensions")
    id("kotlin-android")
    id("com.google.gms.google-services")
}
group = "com.octopus"
version = "1.0-SNAPSHOT"

repositories {
    gradlePluginPortal()
    google()
    jcenter()
    mavenCentral()
}
dependencies {
    implementation(project(":shared"))
    implementation("com.google.android.material:material:1.2.1")
    implementation("androidx.constraintlayout:constraintlayout:2.0.4")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.3.9")
    implementation("io.ktor:ktor-client-android:$ktorVersion")
    implementation("io.ktor:ktor-client-serialization:$ktorVersion")
    implementation("io.ktor:ktor-client-okhttp:$ktorVersion")
    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    implementation("com.google.android.gms:play-services-maps:17.0.0")
    implementation("com.google.android.gms:play-services-location:17.1.0")

    //ML Kit Barcode Scanning
    implementation("com.google.mlkit:barcode-scanning:16.1.0")
    //CameraX Dependencies
    implementation("androidx.camera:camera-core:1.0.0-beta12")
    implementation("androidx.camera:camera-camera2:1.0.0-beta12")
    implementation("androidx.camera:camera-lifecycle:1.0.0-beta12")
    implementation("androidx.camera:camera-view:1.0.0-alpha19")
//    implementation ("androidx.camera:camera-extensions:1.0.0-alpha18")
//    implementation ("com.google.android.material:material:1.3.0-alpha03")
    //Library to get URL Meta Data
    implementation("org.jsoup:jsoup:1.13.1")

    implementation(platform("com.google.firebase:firebase-bom:26.1.1"))
    implementation("com.google.firebase:firebase-database-ktx")


    testImplementation("io.mockk:mockk:1.10.3")
    testImplementation("junit:junit:4.13")
    testImplementation("androidx.arch.core:core-testing:2.1.0")
    testImplementation("io.ktor:ktor-client-mock:$ktorVersion")

//    androidTestImplementation ("org.robolectric:robolectric:4.0.2")
    androidTestImplementation("io.mockk:mockk:1.10.3")
    androidTestImplementation("androidx.test:core:1.3.0")
    androidTestImplementation("com.android.support.test.espresso:espresso-core:3.0.2")
    androidTestImplementation("io.mockk:mockk-android:1.10.3")
    androidTestImplementation("io.ktor:ktor-client-mock:$ktorVersion")

}

android {
    compileSdkVersion(30)
    defaultConfig {
        applicationId = "com.octopus"
        minSdkVersion(23)
        targetSdkVersion(30)
        versionCode = 1
        versionName = "1.0"
    }

    defaultConfig {
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }
    lintOptions {
        disable("Typos")
    }

    testOptions {
//        unitTests.isReturnDefaultValues = true
//        unitTests.isIncludeAndroidResources = true
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }
}