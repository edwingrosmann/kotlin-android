package com.octopus.client_api

import com.octopus.api_client.getChatList
import com.octopus.api_client.getNewChatItems
import com.octopus.client_api.com.octopus.TestBase
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert
import org.junit.Test

class ApiTest : TestBase() {

    @Test
    fun getNewChatItemsTest() {
        Assert.assertThat(runBlocking { getNewChatItems(null) }, equalTo(chatList))
        Assert.assertThat(runBlocking { getNewChatItems(chatList) }, equalTo(updates))
        Assert.assertThat(runBlocking { getNewChatItems(updates) }, equalTo(null))
    }

    @Test
    fun getChatListTest() {
        Assert.assertThat(runBlocking { getChatList() }, equalTo(chatList))
    }
}



