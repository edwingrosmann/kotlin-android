package com.octopus.client_api.com.octopus

import com.octopus.*
import com.octopus.models.ChatItem
import com.octopus.models.Group
import io.ktor.client.*
import io.ktor.client.engine.mock.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.http.*
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.Before

/**
 * This is the base-class for all unit tests that require mocking of the HTTP-API and application context.
 * An API-Endpoint-response can be set as follows:
 *```kotlin
 * when (request.url.encodedPath) {
 *       "/chatList" -> {
 *       val responseHeaders = headersOf("Content-Type" to listOf("application/json"))
 *        respond(Json.encodeToString(chatList), headers = responseHeaders)
 *       }
 * }
 *
```
 */
open class TestBase {

    val chatList = arrayListOf(ChatItem("Octopus", date = 1.0), ChatItem("Didi", date = 1.0))
    val updates =
        arrayListOf(ChatItem("Octopus", desc = "Have you heart?", date = 4.0), ChatItem("Didi", desc = "Let's get cracking", date = 3.0))
    val responseHeaders = headersOf("Content-Type" to listOf("application/json"))

    @Before
    fun setup() {
        mockkStatic("com.octopus.OctopusKt")

        sharedPref = mockk {
            every { getString(any(), any()) } returns "http://localhost:9090"
        }
        appContext = mockk {
            every { getString(any()) } returns "KeyValue"
        }
        userGroup = Group("Waitakere", "10")

        jsonClient = HttpClient(MockEngine) {
            engine {
                addHandler { request ->
                    when (request.url.encodedPath) {
                        "/chatList" -> respond(Json.encodeToString(chatList), headers = responseHeaders)
                        "/chatListFromDate/0" -> respond(Json.encodeToString(chatList), headers = responseHeaders)
                        "/chatListFromDate/1" -> respond(Json.encodeToString(updates), headers = responseHeaders)
                        "/chatListFromDate/4" -> respond("", headers = responseHeaders)
                        else -> error("Unhandled ${request.url}")
                    }
                }
            }

            install(JsonFeature) {
                serializer = KotlinxSerializer(kotlinx.serialization.json.Json {
                    isLenient = false
                    ignoreUnknownKeys = true
                    allowSpecialFloatingPointValues = true
                    useArrayPolymorphism = false
                })
            }
        }

        every {
            logDebug(any(), any())
        } returns Unit

    }
}