package com.octopus.chat

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import android.widget.Toast
import com.octopus.R
import com.octopus.deleteChatListItem
import com.octopus.logDebug
import com.octopus.models.ANONYMOUS
import com.octopus.models.ChatItem
import com.octopus.settings.getDisplayName
import com.octopus.util.showConfirm
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class ChatListAdapter(val items: ArrayList<ChatItem>, context: Activity) : BaseAdapter() {

    private val scope = MainScope()
    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    init {
        items.sortByDescending { it.date }
    }

    override fun getCount(): Int = items.size
    override fun getItem(position: Int): Any = items.get(index = position)
    override fun getItemId(position: Int): Long = position.toLong()
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val view: View?
        val viewHolder: ChatListViewHolder
        if (convertView == null) {
            view = this.layoutInflater.inflate(R.layout.chat_item, parent, false)
            viewHolder = ChatListViewHolder(view)
            view.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ChatListViewHolder
        }

        val item = items.get(index = position)
        viewHolder.displayName.text = item.userName
        viewHolder.desc.text = item.desc
        viewHolder.hash.text = item.hash
        viewHolder.dateString.text = SimpleDateFormat.getDateInstance().format(getDate(position))
        viewHolder.date  = item.date
        return view
    }

    private fun getDate(position: Int): Date = Date(items.get(index = position).date.toLong())

    inner class ChatListViewHolder(row: View) {
        val desc: TextView = row.findViewById(R.id.chatItemDesc)
        val dateString: TextView = row.findViewById(R.id.chatItemDate)
        val hash: TextView = row.findViewById(R.id.chatItemId)
        val displayName: TextView = row.findViewById(R.id.display_name_chat_item)
        var date: Double = 0.0
        init {
            row.setOnLongClickListener {
                val name = displayName.text.toString()
                logDebug(this, "Delete request $name")
                if (name == getDisplayName() || name == ANONYMOUS) {
                    delete(it)
                } else {
                    Toast.makeText(it.context, it.context.getString(R.string.can_only_delete_own), Toast.LENGTH_SHORT).show()
                }
                true
            }
        }

        private fun delete(it: View) {
            val context = it.context as Activity
            context.showConfirm(R.string.delete_this_item, yes = {
                scope.launch(Dispatchers.Main) {
                    deleteChatListItem(ChatItem(userName = displayName.text.toString(), date = date))
                    it.notifyDataChanged()
                }
                Toast.makeText(context, """${context.getString(R.string.deleted)} "${desc.text}" """, Toast.LENGTH_SHORT).show()
            })
        }
    }

    /**
     * Only the Thread in which the view lives, can execute the ```notifyDataSetChanged``` event;
     * So NOT the Coroutine-thread from which this function is called.
     * This is done by using the row's post function!
     **/
    private fun View.notifyDataChanged() = post { notifyDataSetChanged() }
}
