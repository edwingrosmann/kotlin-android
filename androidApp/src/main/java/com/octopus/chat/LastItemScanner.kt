package com.octopus.chat

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import com.octopus.api_client.getLastItem
import com.octopus.api_client.groupFilter
import com.octopus.logDebug
import com.octopus.models.ChatItem
import com.octopus.models.MostRecentData
import com.octopus.settings.getDisplayName
import com.octopus.settings.getRemoteApiUrl
import kotlinx.coroutines.*

const val CONNECTION_ISSUE = "N/A: Please Check the 'Remote Api Url' in the settings and Internet Connection."

///Create A Scanner Singleton...
class LastItemScanner : Service() {

    private var troubleInParadise = false
    private var scope: CoroutineScope? = null
    private val binder: IBinder = MyBinder(this)
    private var started = false

    //The most recent item  and alert entered into the database...
    var lastItem = "Wait for it..."
    var alert = ""
    var alertAcknowledged = true

    class MyBinder(private val l: LastItemScanner) : Binder() {
        fun service(): LastItemScanner {
            return l
        }
    }

    override fun onCreate() {
        logDebug(this, "onCreate Scope: $scope")
        startScanning()
    }

    override fun onDestroy() {
        logDebug(this, "Destroyed")
        stopScanning()
    }

    /**
     * The Last item is pulled into the droid every five seconds.
     */
    private fun startScanning() {

        //Only plunge into a new scanning session when the Coroutine-scope is NOT active...
        if (scopeActive()) return

        prepareScope().launch(Dispatchers.IO) {
            started = true
            logDebug(this, "Start Scanning @ $this")
            while (true) {
                updateLastItemAndAlert()
                troubleInParadise = CONNECTION_ISSUE == lastItem
                logDebug(this@LastItemScanner, "Last Item: $lastItem @ $this")
                delay(5000)
            }
        }
    }

    private suspend fun updateLastItemAndAlert() {

        val mostRecentData = getLastItem("${getRemoteApiUrl()}${ChatItem.endpoints.last}${groupFilter()}")
            ?: MostRecentData(ChatItem(desc = CONNECTION_ISSUE))

        lastItem = mostRecentData.lastItem.desc

        mostRecentData.lastAlert?.let {

            //Only if the alert is new, should it be shown...
            if (newAlertReceived(it)) {
                alert = it.desc
                alertAcknowledged = false
                logDebug(this@LastItemScanner, "ALERT RECEIVED: $alert")
            }
        }
    }


    fun stopScanning() {
        if (scopeActive()) scope?.cancel()
        logDebug(this, "stopScanning Scope status: $scope")
        started = false
    }

    /**
     * Stops the current coroutine and creates a fresh one for the scan-task......
     */
    private fun prepareScope(): CoroutineScope {
        stopScanning()
        scope = MainScope()
        logDebug(this, "New Scanning Scope: $scope")
        return scope!!
    }

    private fun scopeActive() = scope?.isActive == true

    fun isTroubleInParadise() = troubleInParadise

    fun getStatusText() = if (!alertAcknowledged) alert else lastItem

    private fun newAlertReceived(receivedItem: ChatItem) =
        receivedItem.target.individualName == getDisplayName() && alertAcknowledged && alert != receivedItem.desc


    override fun onBind(intent: Intent?): IBinder {
        logDebug(this, "onBind called for $intent")
        return binder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        logDebug(this, "onUnbind called for $intent")
        return true
    }
}
