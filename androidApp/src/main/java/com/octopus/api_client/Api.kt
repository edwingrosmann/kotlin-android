package com.octopus.api_client

import android.util.Log
import com.octopus.jsonClient
import com.octopus.logDebug
import com.octopus.models.ChatItem
import com.octopus.models.MostRecentData
import com.octopus.models.TargetType
import com.octopus.models.UserLocation
import com.octopus.settings.getRemoteApiUrl
import com.octopus.userGroup
import io.ktor.client.request.*
import io.ktor.http.*
import java.util.*

suspend fun getNewChatItems(currentList: ArrayList<ChatItem>?): ArrayList<ChatItem>? {
    return getNullableResponse("${getRemoteApiUrl()}${ChatItem.endpoints.fromDate}/${(currentList?.firstOrNull()?.date ?: 0).toLong()}${groupFilter()}")
}

suspend fun getChatList(): ArrayList<ChatItem>? {
    return getChatList("${getRemoteApiUrl()}${ChatItem.endpoints.all}${groupFilter()}${targetGroupFilter()}")
}


suspend fun getChatList(url: String): ArrayList<ChatItem>? {
    return getNullableResponse(url)
}

suspend fun getLastItem(url: String): MostRecentData? {
    return getNullableResponse(url)
}

suspend fun addChatListItem(chatItem: ChatItem) {
    try {
        jsonClient.post<Unit>(getRemoteApiUrl() + ChatItem.endpoints.all) {
            contentType(ContentType.Application.Json)
            body = chatItem
        }
    } catch (e: Exception) {
        Log.d("Api", "Could not post item ${chatItem}: ${e.message}")
    }
}

suspend fun deleteChatListItem(chatItem: ChatItem) {
    try {
        jsonClient.delete<Unit>("${getRemoteApiUrl()}${ChatItem.endpoints.all}/${chatItem.urlEncodedId()}")
    } catch (e: Exception) {
        Log.d("Api", "Could not delete '$chatItem': $e")
    }
}

suspend fun updateLocation(userLocation: UserLocation) {
    try {
        jsonClient.post<Unit>(getRemoteApiUrl() + UserLocation.endpoints.postLocation) {
            contentType(ContentType.Application.Json)
            body = userLocation
        }
    } catch (e: Exception) {
        Log.d("Api", "Could not post item ${userLocation}: ${e.message}")
    }
}

suspend fun getAllLocations(): List<UserLocation>? {
    return getNullableResponse(getRemoteApiUrl() + UserLocation.endpoints.allUserLocations + groupFilter())
}

suspend inline fun <reified T> getNullableResponse(url: String): T? {
    logDebug("Api", "Requested URL: $url")
    return try {
        jsonClient.get(url)
    } catch (e: Exception) {
        logDebug("Api", "Could not read data from URL '$url': ${e.message}")
        null
    }
}

fun groupFilter() = "?o=${userGroup.organisation}&n=${userGroup.name}"
fun targetGroupFilter() = "&tt=${TargetType.GROUP}"
