package com.octopus.qr

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.mlkit.vision.barcode.Barcode
import com.octopus.R
import com.octopus.cache
import com.octopus.logDebug
import com.octopus.models.BarCodeResultViewData
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.net.URLEncoder
import java.util.concurrent.Executors

class BarcodeResultBottomSheet : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? = inflater.inflate(R.layout.qr_bottom_sheet_barcode_data, container, false)

    fun update(barcode: Barcode, fragmentManager: FragmentManager) {
        logDebug(this, "Barcode type: ${barcode.valueType}")

        if (!isAdded) {
            show(fragmentManager, "")
        }

        when (barcode.valueType) {
            Barcode.TYPE_URL -> {
                updateURL(barcode.url?.url)
            }
            Barcode.TYPE_CONTACT_INFO -> {
                updateContactInfo(barcode.contactInfo)
            }
            Barcode.TYPE_TEXT -> {
                updateText(barcode.displayValue)
            }
        }
    }

    private fun updateURL(url: String?) {
        url?.let {
            //Don't hammer the system with fetching the same url over and over again...
            if (!view.alreadySet(url)) {
                logDebug(this, "Fetching Barcode Data for URL: $it")
                fetchUrlMetaData(it)
            }
        }
    }

    private fun updateContactInfo(info: Barcode.ContactInfo?) {
        view?.apply {
            logDebug(this, "Contact Info: $info")
            titleView().text = name(info)
            descriptionView().text = address(info)
            linkView().text = url(info)
            setButtonText(info)

            linkButton().setOnClickListener { _ ->
                Intent(Intent.ACTION_VIEW).also {
                    it.data = Uri.parse(info?.urls?.firstOrNull() ?: "https://duckduckgo.com/?q=${
                        URLEncoder.encode(name(info) + " " + address(info), "utf-8")
                    }")
                    startActivity(it)
                }
            }
        }
    }

    private fun updateText(info: String?) {
        view?.apply {
            logDebug(this, "Contact Info: $info")
            descriptionView().text = info
            linkButton().visibility = View.GONE
        }
    }

    private fun View?.alreadySet(url: String) = url == this?.linkView()?.text.toString()

    private fun View.linkView() = findViewById<TextView>(R.id.text_view_link)

    private fun View.descriptionView() = findViewById<TextView>(R.id.text_view_desc)

    private fun View.titleView() = findViewById<TextView>(R.id.text_view_title)

    private fun View.linkButton() = findViewById<TextView>(R.id.text_view_visit_link)

    private fun View.setButtonText(info: Barcode.ContactInfo?) {
        linkButton().visibility = View.VISIBLE
        if (url(info) == null) {
            linkButton().text = getString(R.string.search)
        } else {
            linkButton().text = getString(R.string.visit_link)
        }
    }

    //this function will fetch URL data
    private fun fetchUrlMetaData(url: String) {

        //Serve up from cache...
        cache[url]?.let {
            logDebug(this, "Served up from cache: $it")
            populateView(it)
            return
        }

        //Store preliminary cache result to stop consecutive request from also fetching a result when the first request is still in progress...
        cache[url] = BarCodeResultViewData(url)

        //do the hard yards...
        Executors.newSingleThreadExecutor().execute {
            try {
                cache[url] = barCodeResultView(url, Jsoup.connect(url).get())
                populateView(cache[url]!!)
            } catch (e: Throwable) {
                logDebug(this, "Error whilst reading scanned URL: ${e.message}")
            }
        }
    }

    private fun barCodeResultView(url: String, doc: Document) = BarCodeResultViewData(url, doc.title(), description(doc))

    private fun populateView(data: BarCodeResultViewData) {
        this.activity?.runOnUiThread {
            view?.apply {
                linkButton().text = getString(R.string.visit_link)
                linkButton().visibility = View.VISIBLE
                titleView().text = data.title
                descriptionView().text = data.desc
                linkView().text = data.url
                linkButton().setOnClickListener {
                    Intent(Intent.ACTION_VIEW).also { intent ->
                        intent.data = Uri.parse(data.url)
                        startActivity(intent)
                    }
                }
            }
        }
    }

    private fun description(doc: Document): String {
        logDebug(this, "Url Head: ${doc.head().children()}")
        val desc = doc.select("meta[name=description]")
        return if (desc.isNotEmpty()) {
            desc[0].attr("content") ?: NO_DESCRIPTION
        } else NO_DESCRIPTION
    }

    private fun name(info: Barcode.ContactInfo?) = info?.name?.formattedName

    private fun url(info: Barcode.ContactInfo?) = info?.urls?.firstOrNull()

    private fun address(info: Barcode.ContactInfo?): String {
        var a = ""
        info?.addresses?.firstOrNull()?.addressLines?.forEach { a += "$it\n" }
        return a
    }
}

