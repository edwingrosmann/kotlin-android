package com.octopus.qr

import androidx.camera.core.ExperimentalGetImage
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import androidx.fragment.app.FragmentManager
import com.google.mlkit.vision.barcode.Barcode
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.common.InputImage
import io.ktor.util.*

class ImageAnalyzer(private val fragmentManager: FragmentManager) : ImageAnalysis.Analyzer {

    //One bottom sheet that gets reused for each scanned code...
    private var bottomSheet = BarcodeResultBottomSheet()

    @InternalAPI
    @ExperimentalGetImage
    override fun analyze(imageProxy: ImageProxy) {
        imageProxy.image?.let { image ->
            with(BarcodeScanning.getClient().process(InputImage.fromMediaImage(image, imageProxy.imageInfo.rotationDegrees))) {
                addOnCompleteListener {
                    imageProxy.close()
                    if (it.isSuccessful) {
                        readBarcodeData(it.result)
                    } else {
                        it.exception?.printStackTrace()
                    }
                }
            }
        }
    }

    @InternalAPI
    private fun readBarcodeData(barcodes: List<Barcode>) {
        barcodes.lastOrNull()?.let { bottomSheet.update(it, fragmentManager) }
    }
}