package com.octopus.qr

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.AspectRatio
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.lifecycle.LifecycleOwner
import com.octopus.R
import com.octopus.prepareCameraAccess
import com.octopus.util.rotateAnimation
import java.util.concurrent.Executors

const val NO_DESCRIPTION = "No Description Available"

class QrCodeScanner : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.qr_code)
        prepareCameraAccess(this)
        bindPreview(ProcessCameraProvider.getInstance(this).get())
    }

    fun stopQrReader(view: View) = view.rotateAnimation(-3f) { finish() }


    //    @SuppressLint("UnsafeExperimentalUsageError")
    private fun bindPreview(cameraProvider: ProcessCameraProvider) {
        val preview: Preview = Preview.Builder().build()
        val cameraSelector: CameraSelector = CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_BACK).build()
        preview.setSurfaceProvider(previewView().surfaceProvider)

        val imageAnalysis = ImageAnalysis.Builder().setTargetAspectRatio(AspectRatio.RATIO_4_3)
            .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST).build()

        imageAnalysis.setAnalyzer(Executors.newSingleThreadExecutor(), ImageAnalyzer(supportFragmentManager))

        cameraProvider.bindToLifecycle(this as LifecycleOwner, cameraSelector, imageAnalysis, preview)
    }

    private fun previewView(): PreviewView = findViewById(R.id.previewView)
}

