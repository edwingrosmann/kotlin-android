package com.octopus.location

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Binder
import android.os.Bundle
import android.os.HandlerThread
import android.os.IBinder
import com.octopus.hasGpsPermission
import com.octopus.locationManager
import com.octopus.logDebug
import com.octopus.models.UserLocation
import com.octopus.updateCurrentLocation

private const val MIN_DISTANCE_CHANGE_FOR_UPDATES: Float = 1.toFloat()
private const val MIN_TIME_BW_UPDATES = 1000 * 1.toLong()

class LocationTrackService : Service(), LocationListener {
    private val binder: IBinder = MyBinder(this)
    private val listeners = mutableSetOf<LocationListener>()

    override fun onCreate() {
        requestLocationUpdates(this)
    }

    // Register as a LocationUpdate-Listener
    fun addLocationListener(consumer: Context) {
        requestLocationUpdates(consumer as LocationListener)
    }

    //the function "hasPermission" does the permission-check; the Linter fails to acknowledge that...
    @SuppressLint("MissingPermission")
    private fun requestLocationUpdates(listener: LocationListener) {

        //Only continue if this listener is not listening yet and when GPS is available...
        if (!hasGpsPermission() || isListening(this)) return

        //the listener will start receiving location updates now...
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
            MIN_TIME_BW_UPDATES,
            MIN_DISTANCE_CHANGE_FOR_UPDATES,
            listener,
            handlerThread().looper)

        //Keep track of this listener...
        listeners.add(listener)
    }

    private fun handlerThread(): HandlerThread {
        val t = HandlerThread("LocationUpdatesHandler")
        t.start()
        return t
    }

    fun stopListening() {
        stopListening(this)
    }

    fun stopListening(listener: LocationListener) {
        listeners.remove(listener)

        if (hasGpsPermission()) {
            locationManager.removeUpdates(listener)
        }
    }

    /**
     * This method is called when at creation of this service function `updateCurrentLocation` is called
     * Since this is a one-off, this service will stop listening for further location updates.
     */
    override fun onLocationChanged(loc: Location) {
        updateCurrentLocation(UserLocation(loc))
        stopListening()
    }

    override fun onStatusChanged(s: String?, i: Int, bundle: Bundle?) {}
    override fun onProviderEnabled(provider: String) {}
    override fun onProviderDisabled(provider: String) {}

    override fun onBind(intent: Intent?): IBinder {
        logDebug(this, "onBind called for $intent")
        return binder
    }

    fun isListening(listener: LocationListener): Boolean {
        logDebug(this, "All Listeners: $listeners Listener to Check: $listener:  ${listeners.contains(listener)}")
        return listeners.contains(listener)
    }

    class MyBinder(private val s: LocationTrackService) : Binder() {
        fun service(): LocationTrackService {
            return s
        }
    }
}