package com.octopus.location

import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.octopus.*
import com.octopus.R
import com.octopus.models.UserLocation
import com.octopus.settings.getDisplayName
import com.octopus.util.bitmapDescriptor
import com.octopus.util.rotateAnimation

const val MAP_ZOOM: Float = 20f

class LocationViewer : AppCompatActivity(), OnMapReadyCallback, LocationListener {
    private lateinit var mainHandler: Handler
    private lateinit var userMarker: BitmapDescriptor
    private lateinit var otherUsersMarker: BitmapDescriptor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.location)
        GoogleMapOptions().liteMode(true)

        //Make sure that the GPS is activated and that this app has access to it...
        prepareGpsAccess(this)

        locationTrackService.addLocationListener(this)

        //Init all properties...
        userMarker = bitmapDescriptor(getDrawable(R.mipmap.octopus_laucher))
        otherUsersMarker = bitmapDescriptor(getDrawable(R.mipmap.octopus_white_round))
        mainHandler = Handler(Looper.getMainLooper())

        //Fetch all available user-locations and show them on the map...
        initUsers()
        refreshMap()
    }

    override fun onDestroy() {
        super.onDestroy()
        locationTrackService.stopListening(this)
    }

    fun locationRequest(view: View) {
        val dir = if (locationTrackService.isListening(this)) -1f else 1f
        view.rotateAnimation(dir * 2f) {
            view.rotateAnimation(dir * -1f) {
                view.rotateAnimation(dir * 3f) {
                    if (locationTrackService.isListening(this)) {
                        locationTrackService.stopListening(this)
                        (view as Button).text = getString(R.string.enable_tracking)
                        locationLabel().text = ""
                    } else {
                        locationTrackService.addLocationListener(this)
                        (view as Button).text = getString(R.string.disable_tracking)
                    }
                }
            }
        }
    }

    //When the 'Location' button is pressed this activity is ended...
    fun stopTracking(view: View) = view.rotateAnimation(-3f) { finish() }

    override fun onMapReady(googleMap: GoogleMap) {
        with(googleMap) {
            clear()
            allUserLocations.forEach { addMarker(it, otherUsersMarker) }
            currentLocation?.let { addMarker(it, userMarker) }
        }
    }

    private fun GoogleMap.addMarker(loc: UserLocation, icon: BitmapDescriptor): Marker? {
        val pos = LatLng(loc.lat, loc.lon)
        val marker = MarkerOptions().position(pos).title(markerTitle(loc)).icon(icon)
        animateCamera(CameraUpdateFactory.newLatLng(pos))
        animateCamera(CameraUpdateFactory.newLatLngZoom(pos, MAP_ZOOM))
        return addMarker(marker)
    }

    private fun markerTitle(loc: UserLocation) = if (loc.userName == getDisplayName()) "${loc.userName} (You)" else loc.userName

    fun refreshMap() {
        runOnUiThread {
            map().getMapAsync(this@LocationViewer)
        }
    }

    private fun locationLabel() = findViewById<TextView>(R.id.location_lbl)
    private fun map() = supportFragmentManager.findFragmentById(R.id.my_map) as SupportMapFragment

    override fun onLocationChanged(location: Location) {
        mainHandler.post {
            val userLocation = UserLocation(location)
            locationLabel().text = userLocation.toString()
            updateCurrentLocation(userLocation)
//            updateUsers()
            refreshMap()
        }
    }

    override fun onStatusChanged(s: String?, i: Int, bundle: Bundle?) {}
    override fun onProviderEnabled(provider: String) {}
    override fun onProviderDisabled(provider: String) {}
}