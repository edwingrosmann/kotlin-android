package com.octopus.settings

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.SharedPreferences
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import com.octopus.R
import com.octopus.api_client.getChatList
import com.octopus.api_client.getLastItem
import com.octopus.api_client.groupFilter
import com.octopus.appContext
import com.octopus.models.ANONYMOUS
import com.octopus.models.ChatItem
import com.octopus.sharedPref
import com.octopus.updateUserGroup

fun getOrganisationName(): String {
    return sharedPref.getString(appContext.getString(R.string.saved_organisation_name_key),appContext.getString(R.string.default_organisation_name))!!
}

fun getGroupName(): String {
    return sharedPref.getString(appContext.getString(R.string.saved_group_name_key), appContext.getString(R.string.default_group_name))!!
}

fun getRemoteApiUrl(): String {
    return sharedPref.getString(appContext.getString(R.string.saved_remote_api_url_key),
        appContext.getString(R.string.default_remote_api_url))!!
}

fun getDisplayName(): String {
    return sharedPref.getString(appContext.getString(R.string.saved_display_name), "")!!
}

fun getShowLastItem(): Boolean {
    return sharedPref.getBoolean(appContext.getString(R.string.saved_show_last_item), false)
}

fun saveDisplayName(view: TextView) {
    val value = if (view.text.isNotBlank()) view.text else ANONYMOUS
    saveSetting(sharedPref, appContext.getString(R.string.saved_display_name), value)
}

fun saveGroupName(view: TextView) {
    saveSetting(sharedPref, appContext.getString(R.string.saved_group_name_key), view.text)
    updateUserGroup()
}

fun saveOrganisationName(view: TextView) {
    saveSetting(sharedPref, appContext.getString(R.string.saved_organisation_name_key), view.text)
    updateUserGroup()
}

fun saveShowLastItem(view: CheckBox) {
    saveSetting(sharedPref, appContext.getString(R.string.saved_show_last_item), view.isChecked)
}

suspend fun saveRemoteUrl(view: TextView, context: Activity) {
    if (validateUrl(view, context)) {
        saveSetting(sharedPref, appContext.getString(R.string.saved_remote_api_url_key), view.text)
    }
}

fun setBluetoothName(name: String) {
    BluetoothAdapter.getDefaultAdapter()?.name = name
}

suspend fun validateUrl(view: TextView, activity: Activity): Boolean {
    var isValid = false
    try {
        //Check if the chat-list and the last item can be retrieved given the base-url provided...
        getChatList("${view.text}${ChatItem.endpoints.all}${groupFilter()}")!!
        getLastItem("${view.text}${ChatItem.endpoints.last}${groupFilter()}")!!

        //say thanks...
        activity.runOnUiThread {
            view.setBackgroundColor(appContext.getColor(android.R.color.background_light))
            Toast.makeText(view.context, "That's a valid API URL!", Toast.LENGTH_LONG).show()
        }
        isValid = true

    } catch (e: Exception) {
        e.printStackTrace()
        activity.runOnUiThread {
            view.setBackgroundColor(appContext.getColor(android.R.color.holo_red_light))
            Toast.makeText(appContext, "Invalid API URL or bad internet Connection", Toast.LENGTH_LONG).show()
        }
    }
    return isValid
}

/**
 * saves the value provided the the application's 'Settings.xml' locally on the Android-device;
 * It knows how to store Strings, Longs, Ints Floats and Booleans
 */
fun <T> saveSetting(sharedPref: SharedPreferences, key: String, value: T) = with(sharedPref.edit()) {
    if (value is String) {
        putString(key, value)
    } else if (value is Long) {
        putLong(key, value)
    } else if (value is Int) {
        putInt(key, value)
    } else if (value is Float || value is Double) {
        putFloat(key, value as Float)
    } else if (value is Boolean) {
        putBoolean(key, value)
    } else {
        putString(key, value.toString())
    }
    commit()
}
