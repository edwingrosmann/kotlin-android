package com.octopus.settings

import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputEditText
import com.octopus.*
import com.octopus.util.rotateAnimation
import com.octopus.util.setEditCompleteAction
import com.octopus.util.showConfirm
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class Settings : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings)

        //Initialize the fields with persisted or default values...
        remoteApiUrl().setText(getRemoteApiUrl())
        organisation().setText(getOrganisationName())
        group().setText(getGroupName())
        displayName().setText(getDisplayName())
        showLastItem().isChecked = getShowLastItem()

        remoteApiUrl().setEditCompleteAction {
            MainScope().launch(Dispatchers.IO) { validateUrl(it, this@Settings) }
        }
    }

    //When the 'Save-and-Return' button is pressed, all fields are saved, and then this activity is ended...
    fun saveAll(view: View) {
        showConfirm(R.string.save_all_question, no = { finish() }, yes = {
            MainScope().launch(Dispatchers.IO) {
                saveRemoteUrl(remoteApiUrl(), this@Settings)
            }
            saveDisplayName(displayName())
            saveGroupName(group())
            saveOrganisationName(organisation())
            saveShowLastItem(showLastItem())
            runOnUiThread {
                view.rotateAnimation(-3f) { finish() }
            }
        })
    }

    private fun displayName() = findViewById<TextInputEditText>(R.id.display_name)
    private fun remoteApiUrl() = findViewById<TextInputEditText>(R.id.remote_api_url)
    private fun organisation() = findViewById<TextInputEditText>(R.id.organisation_value)
    private fun group() = findViewById<TextInputEditText>(R.id.group_value)
    private fun showLastItem() = findViewById<CheckBox>(R.id.show_last_item)
}