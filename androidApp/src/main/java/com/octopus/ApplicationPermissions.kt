package com.octopus

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.provider.Settings
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import com.octopus.util.showConfirm

private const val ALL_PERMISSIONS_RESULT = 101

fun prepareCameraAccess(activity: Activity) {
    if(!hasCameraPermission()){
        requestPermissions(activity, arrayOf(Manifest.permission.CAMERA))
        activity.finish()
    }
}

fun prepareGpsAccess(activity: Activity) {
    try {
        if (!gpsEnabled()) {
            Toast.makeText(activity, "No Location Provider Available", Toast.LENGTH_SHORT).show()
            showEnableGpsDialog(activity)
        } else if (!hasGpsPermission()) {
            requestPermissions(activity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION))
            activity.finish()
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun requestPermissions(context: Activity, requiredPermissions: Array<String>) {
    val permissionsToRequest = notGrantedPermissions(requiredPermissions)
    if (permissionsToRequest.isNotEmpty()) requestPermissions(context, permissionsToRequest, ALL_PERMISSIONS_RESULT)
}

fun showEnableGpsDialog(activity: Activity) {
    activity.showConfirm(message = R.string.enable_gps, no = { activity.finish() }) {
        activity.finish()
        activity.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
    }
}

fun hasGpsPermission() = hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)
fun hasCameraPermission() = hasPermission(Manifest.permission.CAMERA)

private fun gpsEnabled() = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)

private fun hasPermission(permission: String): Boolean {
    return ActivityCompat.checkSelfPermission(appContext, permission) == PackageManager.PERMISSION_GRANTED
}

private fun notGrantedPermissions(required: Array<String>): Array<String> {
    return required.filter { !hasPermission(it) }.toTypedArray()
}