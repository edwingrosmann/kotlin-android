package com.octopus

import com.google.firebase.database.*
import com.google.firebase.database.ktx.getValue
import com.octopus.models.ChatItem
import com.octopus.models.UpdateType
import com.octopus.models.UserLocation
import kotlinx.coroutines.delay

fun userGroupMembersQuery() = userLocationCollection.orderByChild("group/id").equalTo(userGroup.id)
fun chatItemsQuery() = chatItemsCollection.orderByChild("group/id").equalTo(userGroup.id)

suspend fun getAllLocations(): ArrayList<UserLocation> {
    return fetchAll(userGroupMembersQuery())
}

fun updateLocation(userLocation: UserLocation) {
    userLocationCollection.child(userLocation.userName).setValue(userLocation)
}

suspend fun getNewChatItems(currentList: ArrayList<ChatItem>?): ArrayList<ChatItem> {
    return arrayListOf(*getChatList().filter { it.date > (currentList?.firstOrNull()?.date ?: 0).toLong() }.toTypedArray())
}

suspend fun getChatList(): ArrayList<ChatItem> {
    return fetchAll(chatItemsQuery())
}

fun addChatListItem(chatItem: ChatItem) {
    chatItemsCollection.child(chatItem.id).setValue(chatItem)
}

fun deleteChatListItem(chatItem: ChatItem) {
    chatItemsCollection.child(chatItem.id).removeValue()
}

fun listenForNewChatItems(callback: (ArrayList<ChatItem>, action: UpdateType) -> Unit) {
    listenForNewData(chatItemsQuery(), callback)
}

fun listenForUpdatedUserLocations(callback: (ArrayList<UserLocation>, action: UpdateType) -> Unit) {
    listenForNewData(userGroupMembersQuery(), callback)
}

/**
 * Connects a callback-function with data-updates; the callback receives the list-of-data and the type of update(add, remove or update)
 */
private inline fun <reified T : Any> listenForNewData(query: Query, crossinline callback: (ArrayList<T>, action: UpdateType) -> Unit) {

    query.addChildEventListener(object : ChildEventListener {
        override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
            println("Firebase Received NEW Data:${T::class.java.simpleName}: ${snapshot.getValue<T>()}")
            callback(arrayListOf(snapshot.getValue<T>()!!), UpdateType.ADD)
        }

        override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            println("Firebase Received UPDATED Data:${T::class.java.simpleName}: ${snapshot.getValue<T>()}")
            callback(arrayListOf(snapshot.getValue<T>()!!), UpdateType.UPDATE)
        }

        override fun onChildRemoved(snapshot: DataSnapshot) {
            println("Firebase Deleted Data:${T::class.java.simpleName}: ${snapshot.getValue<T>()}")
            callback(arrayListOf(snapshot.getValue<T>()!!), UpdateType.REMOVE)
        }

        override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
            println("Firebase Received MOVED Data:${T::class.java.simpleName}: ${snapshot.getValue<T>()}")
            callback(arrayListOf(snapshot.getValue<T>()!!), UpdateType.UPDATE)
        }

        override fun onCancelled(error: DatabaseError) {
            println("Firebase Could not receive item of type '${T::class.java.simpleName}': $error")

        }
    })
}

/**
 * Waits for- and then returns the data as per the query provided.
 */
private suspend inline fun <reified T : Any> fetchAll(query: Query): ArrayList<T> {
    var data = arrayListOf<T>()
    var dataReady = false
    val timeMillis = 100L
    val maxWaitCounter = 5000 / timeMillis
    var waitCounter = 0

    query.addListenerForSingleValueEvent(object : ValueEventListener {
        override fun onDataChange(snapshot: DataSnapshot) {
            println("Firebase Fetched Raw Data: ${snapshot.value}")
            snapshot.children.forEach {
                println("Firebase ${T::class.java.simpleName}: ${it.getValue<T>()}")
            }
            if (snapshot.hasChildren()) {
                data = arrayListOf(*snapshot.getValue<Map<String, T>>()!!.values.toTypedArray())
            }
            dataReady = true
        }

        override fun onCancelled(error: DatabaseError) {
            println("Firebase Could not fetch items of type '${T::class.java.simpleName}': $error")
            dataReady = true
        }
    })

    while (!dataReady && waitCounter++ < maxWaitCounter) {
        println("Firebase waiting for ${T::class.java.simpleName} $waitCounter / $maxWaitCounter...")
        delay(timeMillis)
    }

    return data
}

