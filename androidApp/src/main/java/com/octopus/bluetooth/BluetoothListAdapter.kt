package com.octopus.bluetooth

import android.annotation.SuppressLint
import android.app.Activity
import android.icu.text.SimpleDateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.octopus.R
import com.octopus.addChatListItem
import com.octopus.logDebug
import com.octopus.models.BluetoothItem
import com.octopus.models.ChatItem
import com.octopus.models.Target
import com.octopus.models.TargetType
import com.octopus.settings.getDisplayName
import com.octopus.userGroup
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.util.*

class BluetoothListAdapter(private val items: MutableList<BluetoothItem>, context: Activity) : BaseAdapter() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    init {
        items.sortBy { "${it.date / 1000} ${it.name}" }
    }

    override fun getCount(): Int = items.size
    override fun getItem(position: Int): Any = items[position]
    override fun getItemId(position: Int): Long = position.toLong()

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {

        val bluetoothItem = items[position]
        logDebug(this, "Bluetooth Item Shown: $bluetoothItem")

        return layoutInflater.inflate(R.layout.bluetooth_item, parent, false).apply {
            BluetoothListViewHolder(this).apply {
                displayName.text = bluetoothItem.name
                date.text = SimpleDateFormat.getDateTimeInstance().format(getDate(position))
            }
            if (bluetoothItem.present) setBackgroundColor(ContextCompat.getColor(context, R.color.green_light))
        }
    }

    private fun getDate(position: Int): Date = Date(items[position].date)

    inner class BluetoothListViewHolder(row: View) {
        val date: TextView = row.findViewById(R.id.bluetoothItemDate)
        val displayName: TextView = row.findViewById(R.id.display_name_bluetooth_item)

        init {
            row.setOnLongClickListener {
                val name = displayName.text.toString()
                logDebug(this, "Request attention from $name")
                MainScope().launch {
                    addChatListItem(ChatItem(userName = getDisplayName(),
                        desc = "At ${Date()} ${getDisplayName()} sent you a message: 'Please Contact your Group Lead Now $name.'",
                        date = Date().time.toDouble(),
                        group = userGroup,
                        target = Target(TargetType.INDIVIDUAL, name)))
                    Toast.makeText(it.context, "${it.context.getString(R.string.attention_request_sent)} to $name", Toast.LENGTH_SHORT)
                        .show()
                }
                true
            }
        }
    }
}

