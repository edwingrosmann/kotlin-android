package com.octopus.bluetooth

import android.app.*
import android.bluetooth.BluetoothAdapter
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.os.ParcelUuid
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.PRIORITY_LOW
import com.octopus.*
import com.octopus.settings.getDisplayName
import com.octopus.settings.setBluetoothName

const val CHANNEL_ID = "Bluetooth Low Energy"
const val NOTIFICATION_ID: Int = 12

class BluetoothLeService : Service() {
    var started = false
    private val binder: IBinder = MyBinder(this)
    private val advertiseSettings: AdvertiseSettings =
        AdvertiseSettings.Builder().setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED).setConnectable(true).setTimeout(0)
            .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM).build()
    private val advertiseData: AdvertiseData =
        AdvertiseData.Builder().setIncludeDeviceName(true).setIncludeTxPowerLevel(true).addServiceUuid(ParcelUuid(TimeProfile.TIME_SERVICE))
            .build()
    private val bluetoothReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF)) {
                BluetoothAdapter.STATE_ON -> {
                    advertise()
                }
                BluetoothAdapter.STATE_OFF -> {
                    stopAdvertising()
                }
            }
        }
    }
    private val advertiseCallback = object : AdvertiseCallback() {
        override fun onStartSuccess(settingsInEffect: AdvertiseSettings) {
            logDebug(this@BluetoothLeService, "LE Advertise Started with settings: $settingsInEffect")
            started = true
        }

        override fun onStartFailure(errorCode: Int) {
            logDebug(this@BluetoothLeService, "LE Advertise Failed: $errorCode")
            started = false
        }
    }

    class MyBinder(private val s: BluetoothLeService) : Binder() {
        fun service(): BluetoothLeService {
            return s
        }
    }

    override fun onCreate() {
        // Register for system Bluetooth events and then try to start Advertising... ..
        registerBluetoothEvents()
        advertise()
    }

    override fun onBind(intent: Intent?): IBinder {
        logDebug(this, "onBind called for $intent")
        return binder
    }

    private fun registerBluetoothEvents() {
        appContext.registerReceiver(bluetoothReceiver, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))
    }

    fun advertise() {
        //Initialize the BluetoothAdapter...
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        //Only continue when Bluetooth LE is available...
        if (!hasBluetoothLeSupport(bluetoothAdapter)) return

        //Set the Bluetooth name...
        setBluetoothName(getDisplayName())

        startAdvertisingOrEnable()
    }

    private fun startAdvertisingOrEnable() {
        if (!bluetoothAdapter.isEnabled) {
            logDebug(this, "Bluetooth is currently disabled...enabling")
            bluetoothAdapter.enable()
        } else {
            logDebug(this, "Bluetooth start advertising...")
            startAdvertising()
        }
    }

    private fun hasBluetoothLeSupport(bluetoothAdapter: BluetoothAdapter?): Boolean {

        if (bluetoothAdapter == null) {
            logDebug(this, "Bluetooth is not supported")
            return false
        } else if (!appContext.packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            logDebug(this, "Bluetooth LE is not supported")
            return false
        }
        return true
    }

    private fun startAdvertising() {
        //Only start when not started yet...
        if (started) return

        bluetoothAdapter.bluetoothLeAdvertiser?.let {
            it.stopAdvertising(advertiseCallback)
            it.startAdvertising(advertiseSettings, advertiseData, advertiseCallback)
            logDebug(this, "Bluetooth LE Advertiser Service Started")
        } ?: logDebug(this, "Failed to START Bluetooth LE Advertiser")

        startInForeground()
    }

    fun stopAdvertising() {
        started = false
        bluetoothAdapter.bluetoothLeAdvertiser?.let {
            it.stopAdvertising(advertiseCallback)
            logDebug(this, "Bluetooth LE  Advertiser Service Stopped")
        } ?: logDebug(this, "Failed to STOP Bluetooth LE Advertiser")
    }

    private fun startInForeground() {
        startForeground(NOTIFICATION_ID,
            NotificationCompat.Builder(this, prepareChannelId()).setOngoing(true).setPriority(PRIORITY_LOW)
                .setCategory(Notification.CATEGORY_SERVICE).setSmallIcon(R.drawable.ic_octopus_white)
                .setContentTitle(getString(R.string.notification_title)).setContentText("Your Bluetooth Name is '${getDisplayName()}'")
                .setContentIntent(PendingIntent.getActivity(this, 0, Intent(this, MainActivity::class.java), 0)).build())
    }

    private fun prepareChannelId() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        createNotificationChannel(CHANNEL_ID, CHANNEL_ID)
    } else {
        CHANNEL_ID
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(NotificationChannel(channelId,
            channelName,
            NotificationManager.IMPORTANCE_NONE).also {
            it.lightColor = Color.BLUE
            it.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        })
        return channelId
    }
}