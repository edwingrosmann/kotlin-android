package com.octopus.bluetooth

import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.octopus.*
import com.octopus.models.BluetoothDevices
import com.octopus.models.BluetoothItem
import com.octopus.util.rotateAnimation
import kotlinx.coroutines.*

private const val REQUEST_DISCOVERABLE_BT: Int = 102

class BluetoothScanner : AppCompatActivity() {
    private var scope = MainScope()
    private var periodicScanningEnabled = true

    private val devices = BluetoothDevices().apply {
        btBondedDevices.addAll(bluetoothAdapter.bondedDevices)
        allKnownDevices.addAll(othersNames.map { BluetoothItem(it) })
    }

    // Create a BroadcastReceiver for ACTION_FOUND...
    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            printBroadcastReceivedIntent(action, intent)

            when (action) {
                BluetoothDevice.ACTION_FOUND -> {
                    devices.add(intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE))
                    updateUi()
                }
            }
        }

        private fun printBroadcastReceivedIntent(action: String?, intent: Intent) {
            logDebug(this@BluetoothScanner,
                "**************************BT Dev EXTRA INFO START for action $action******************************")
            intent.extras?.let { it.keySet().forEach { i -> logDebug(this@BluetoothScanner, "BT Device extra info: $i=${it.get(i)}") } }
            logDebug(this@BluetoothScanner, "**************************END********************************")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bluetooth)
        updateUi()
        registerReceiver(receiver, IntentFilter(BluetoothDevice.ACTION_FOUND))
        scanPeriodically()
    }

    override fun onDestroy() {
        super.onDestroy()
        scope.cancel()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_DISCOVERABLE_BT -> {
                logDebug(this, "Bluetooth Discoverable for $resultCode seconds.")
                if (resultCode != 0) {
                    periodicScanningEnabled = true
                }
            }
        }
    }

    fun stopScanning(view: View) = view.rotateAnimation(-3f) { finish() }
    fun rescan(view: View) = view.rotateAnimation {
        startBtDiscovery()
        updateUi()
    }

    private fun scanPeriodically() {
        scope.launch {
            delay(2000)
            while (true) {
                logDebug(this@BluetoothScanner, "Scan Periodically: $this")
                if (periodicScanningEnabled) clickRescanBtButton()
                delay(10000)
            }
        }
    }

    private fun startBtDiscovery() {
        bluetoothAdapter.cancelDiscovery()
        bluetoothAdapter.startDiscovery()
    }

    private fun devicesList() = findViewById<TextView>(R.id.devicesList)
    private fun listView(): ListView = findViewById(R.id.bluetoothListView)
    private fun rescanButton(): Button = findViewById(R.id.rescanBt)
    private fun clickRescanBtButton() = runOnUiThread { rescanButton().performClick() }

    private fun updateUi() {
        runOnUiThread {
            listView().adapter = BluetoothListAdapter(getBluetoothList(), this@BluetoothScanner)

            devicesList().text = "Discovered Devices:"
            devices.btDevices.forEach {
                logDebug(this, "BT Device found: ${it.name}/${it.address}")
                devicesList().text = "${devicesList().text}\n${it.name}/${it.address}"
            }
            devicesList().text = "${devicesList().text}\n\nUnknown Discovered Devices:"
            devices.btUnknownDevices.forEach {
                logDebug(this, "Unknown BT Device found: ${it.name}/${it.address}")
                devicesList().text = "${devicesList().text}\n${it.name}/${it.address}"
            }
            devicesList().text = "${devicesList().text}\n\nBluetooth LE Devices:"
            devices.btLeDevices.forEach {
                logDebug(this, "BT LE Device found: ${it.name}/${it.address}")
                devicesList().text = "${devicesList().text}\n${it.name}/${it.address}"
            }
            devicesList().text = "${devicesList().text}\n\nBonded Devices:"
            devices.btBondedDevices.forEach {
                logDebug(this, "BT Bonded Device found: ${it.name}/${it.address}")
                devicesList().text = "${devicesList().text}\n${it.name}/${it.address}"
            }
        }
    }

    private fun getBluetoothList(): MutableList<BluetoothItem> {
        return devices.allKnownDevices.toMutableList()
    }
}



