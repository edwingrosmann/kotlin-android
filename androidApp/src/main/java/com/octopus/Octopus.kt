package com.octopus

import android.annotation.SuppressLint
import android.app.Application
import android.app.Service
import android.bluetooth.BluetoothAdapter
import android.content.*
import android.location.LocationManager
import android.os.IBinder
import android.util.Log
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.octopus.bluetooth.BluetoothLeService
import com.octopus.chat.LastItemScanner
import com.octopus.location.LocationTrackService
import com.octopus.models.BarCodeResultViewData
import com.octopus.models.Group
import com.octopus.models.UserLocation
import com.octopus.settings.getDisplayName
import com.octopus.settings.getGroupName
import com.octopus.settings.getOrganisationName
import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

lateinit var appContext: Context
lateinit var sharedPref: SharedPreferences
lateinit var userGroup: Group
lateinit var bluetoothAdapter: BluetoothAdapter
lateinit var bluetoothLeService: BluetoothLeService
lateinit var locationTrackService: LocationTrackService
lateinit var lastItemScanner: LastItemScanner
lateinit var locationManager: LocationManager
lateinit var userLocationCollection: DatabaseReference
lateinit var chatItemsCollection: DatabaseReference

var cache = HashMap<String, BarCodeResultViewData>()
var currentLocation: UserLocation? = null
var allUserLocations = arrayListOf<UserLocation>()
var othersNames = listOf<String>()

var jsonClient = HttpClient(Android) {
    engine {
        connectTimeout = 100_000
        socketTimeout = 100_000
//        proxy = Proxy(Proxy.Type.HTTP, InetSocketAddress("localhost", serverPort))
    }

    install(JsonFeature) {
        serializer = KotlinxSerializer(kotlinx.serialization.json.Json {
            isLenient = false
            ignoreUnknownKeys = true
            allowSpecialFloatingPointValues = true
            useArrayPolymorphism = false
        })
    }
}

@SuppressLint("NeverUsed")
class Octopus : Application() {

    private val btLeServiceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {}
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            bluetoothLeService = (service as BluetoothLeService.MyBinder).service()
        }
    }

    private val lastItemScannerServiceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {}
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            lastItemScanner = (service as LastItemScanner.MyBinder).service()
        }
    }

    private val locationTrackServiceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {}
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            locationTrackService = (service as LocationTrackService.MyBinder).service()
            logDebug(this, "LocationTrackService initialised: $locationTrackService")
        }
    }

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
        appContext.setTheme(R.style.AppTheme)
        sharedPref = appContext.getSharedPreferences("Settings", Context.MODE_PRIVATE)
        locationManager = appContext.getSystemService(Service.LOCATION_SERVICE) as LocationManager
        userLocationCollection = Firebase.database.getReference("userLocation")
        chatItemsCollection = Firebase.database.getReference("chatItems")
        updateUserGroup()
        initUsers()
        startLocationTrackService()
        startBluetoothLeService()
        startLastItemScannerService()
    }

    private fun startBluetoothLeService() {
        val intent = Intent(this, BluetoothLeService::class.java)
        appContext.startService(intent)
        appContext.bindService(intent, btLeServiceConnection, BIND_NOT_FOREGROUND)
    }

    private fun startLastItemScannerService() {
        val intent = Intent(this, LastItemScanner::class.java)
        appContext.startService(intent)
        appContext.bindService(intent, lastItemScannerServiceConnection, BIND_AUTO_CREATE)
    }

    private fun startLocationTrackService() {
        val intent = Intent(this, LocationTrackService::class.java)
        appContext.startService(intent)
        appContext.bindService(intent, locationTrackServiceConnection, BIND_AUTO_CREATE)
    }
}

fun initUsers() {
    MainScope().launch(Dispatchers.IO) {
        allUserLocations = arrayListOf(*getAllLocations().filter { it.userName != currentLocation?.userName }.toTypedArray())
        othersNames = allUserLocations.filter { it.userName != getDisplayName() }.map { it.userName }
    }
}

fun updateUserGroup() {
    userGroup = Group(getOrganisationName(), getGroupName())
}

fun updateCurrentLocation(loc: UserLocation) {
    currentLocation = loc
    updateLocation(loc)
}

fun logDebug(caller: Any, msg: String) {
    Log.d(caller.javaClass.simpleName, msg)
}