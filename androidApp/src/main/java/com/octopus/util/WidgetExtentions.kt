package com.octopus.util

import android.R.string.cancel
import android.R.string.ok
import android.app.Activity
import android.app.AlertDialog
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.view.View
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory

/**
 * This function will rotate a button 360deg around its center in half a second: After that the closure-function provided will be executed.
 * e.g.
 *
 * ```kotlin
 * myButton = Button()
 * myButton.rotateAnimation(){startActivity(Intent(context, Settings::class.java)) }
 * ```
 */
fun View.rotateAnimation(spinFactor: Float = 1.0f, action: () -> Unit) {

    startAnimation(RotateAnimation(0.0f,
        spinFactor * 360.0f,
        Animation.ABSOLUTE,
        width.toFloat() / 2.0f,
        Animation.ABSOLUTE,
        height.toFloat() / 2.0f).apply {
        duration = 500
        repeatCount = 0
        setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}
            override fun onAnimationStart(animation: Animation?) {}
            override fun onAnimationEnd(animation: Animation?) {
                action()
            }
        })
    })
}

/**
 * This Activity extension shows a Yes/No Dialog; A Yes and a No Lambda function are to be provided.
 * E.g. WITHIN an Activity-class
 *
 * ```kotlin
 *     //When the 'Save-and-Return' button is pressed, all fields are saved, and then this activity is ended...
 *    fun saveAll(view: View) {
 *       showConfirm(R.string.save_all_question
 *      ,{finish()}){
 *      saveYear(schoolYear())
 *      saveRemoteUrl(remoteApiUrl())
 *      view.rotateAnimation(-3) { finish() }
 *     }
 *   }
 * ```
 */
fun Activity.showConfirm(message: Int, no: () -> Unit = {}, yes: () -> Unit = {}) {
    AlertDialog.Builder(this).setTitle(message).setCancelable(false).setPositiveButton(ok) { _, _ ->
        yes()
    }.setNegativeButton(cancel) { _, _ ->
        no()
    }.create().show()
}

/**
 * This TextEdit extension registers an 'OnEditorActionListener'
 * The 'action' closure can be any code that needs executing when the event occurs; this closure code has the TextEdit-view available to it as parameter 'it'
 * e.g:
 *
 * ```kotlin
 *   val remoteApiUrl = TextView(context)
 *   remoteApiUrl.setEditCompleteAction {
 *     logDebug(this, "The text field has value ${it.text}")
 *   }
 *```
 *
 */
fun TextView.setEditCompleteAction(action: (t: TextView) -> Unit) {
    setOnEditorActionListener { v, _, _ ->
        hideKeyboard(this)
        action(v)
        this.clearFocus()
        true
    }
}

fun hideKeyboard(view: View) {
    (view.context.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken,
        0)
}

fun bitmapDescriptor(image: Drawable?): BitmapDescriptor {
    image!!.setBounds(0, 0, image.intrinsicWidth, image.intrinsicHeight)
    val bitmap = Bitmap.createBitmap(image.intrinsicWidth, image.intrinsicHeight, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    image.draw(canvas)
    return BitmapDescriptorFactory.fromBitmap(bitmap)
}