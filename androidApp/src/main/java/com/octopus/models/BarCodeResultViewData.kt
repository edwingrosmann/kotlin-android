package com.octopus.models

data class BarCodeResultViewData(val url: String, val title: String = "", val desc: String = "") {
    override fun toString(): String {
        return "Url=$url, title=$title, description=$desc"
    }
}