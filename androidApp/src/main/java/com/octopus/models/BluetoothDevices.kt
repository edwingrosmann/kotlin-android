package com.octopus.models

import android.bluetooth.*
import android.bluetooth.BluetoothDevice.DEVICE_TYPE_LE
import com.octopus.appContext
import com.octopus.bluetooth.TimeProfile
import com.octopus.logDebug
import com.octopus.othersNames
import java.util.*

private const val STATE_DISCONNECTED = 0
private const val STATE_CONNECTING = 1
private const val STATE_CONNECTED = 2

const val ACTION_GATT_CONNECTED = "com.example.bluetooth.le.ACTION_GATT_CONNECTED"
const val ACTION_GATT_DISCONNECTED = "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED"
const val ACTION_GATT_SERVICES_DISCOVERED = "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED"
const val ACTION_DATA_AVAILABLE = "com.example.bluetooth.le.ACTION_DATA_AVAILABLE"
const val EXTRA_DATA = "com.example.bluetooth.le.EXTRA_DATA"
const val TIME_SERVER_UPDATE = "00002a37-0000-1000-8000-00805f9b34fb"
const val CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb"

class BluetoothDevices {
    var btBondedDevices = mutableListOf<BluetoothDevice>()
    var btDevices = mutableListOf<BluetoothDevice>()
    var btUnknownDevices = mutableListOf<BluetoothDevice>()
    var btLeDevices = mutableListOf<BluetoothDevice>()
    var allKnownDevices = mutableSetOf<BluetoothItem>()

    fun add(device: BluetoothDevice?) {
        device?.let { i ->

            if (device.type == DEVICE_TYPE_LE) {
                btLeDevices.replace(device)
                val bluetoothGatt = device.connectGatt(appContext, true, mGattCallback)
                logDebug(this, "Bluetooth Connected to GATT: ${device.name}: $bluetoothGatt; UUID: ${
                    bluetoothGatt.services.map {
                        it.uuid
                    }
                } TIME Service: ${bluetoothGatt.getService(TimeProfile.TIME_SERVICE)}")
            }

            if (othersNames.contains(i.name)) {
                btDevices.replace(i)
                addToAllKnownDevices(i)
            } else {
                btUnknownDevices.replace(i)
            }
        }
    }

    private fun MutableList<BluetoothDevice>.replace(i: BluetoothDevice) {

        //Remove all devices with the same name and add the new device...
        this.removeAll(this.filter { it.name == i.name })
        this.add(i)

        //Sort the resulting list...
        val sl = this.sortedBy { it.name }

        //empty the list and add the sorted list back in...
        this.clear()
        this.addAll(sl)
    }

    private fun addToAllKnownDevices(i: BluetoothDevice): Boolean {
        removeFromAllKnownDevices(i)
        return allKnownDevices.add(BluetoothItem(i.name, true))
    }

    private fun removeFromAllKnownDevices(i: BluetoothDevice) {
        val item = BluetoothItem(i.name)
        if (allKnownDevices.contains(item) && !allKnownDevices.first { it.name == i.name }.present) {
            allKnownDevices.remove(item)
        }
    }

    private val mGattCallback: BluetoothGattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
            var intentAction = ""
            logDebug(this@BluetoothDevices, "Connected to GATT server: $gatt; Status: $status, new State: $newState ")

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                intentAction = ACTION_GATT_CONNECTED
                logDebug(this@BluetoothDevices, "Connected to GATT server.")
                logDebug(this@BluetoothDevices, "Attempting to start service discovery: ${gatt.discoverServices()}")
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED
                logDebug(this@BluetoothDevices, "Disconnected from GATT server.")
            }
            logDebug(this,"Intent Action = $intentAction")
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            logDebug(this@BluetoothDevices, "onServicesDiscovered received: $status")
            if (status == BluetoothGatt.GATT_SUCCESS) {
//                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED)
            }
        }

        override fun onCharacteristicRead(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            status: Int,
        ) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
//                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic)
            }
        }

        override fun onCharacteristicChanged(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
        ) {
//            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic)
        }
    }
}

