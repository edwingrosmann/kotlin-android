package com.octopus.models

import android.location.Location
import com.google.firebase.database.IgnoreExtraProperties
import com.octopus.settings.getDisplayName
import com.octopus.userGroup
import kotlinx.serialization.Serializable
import java.util.*

const val ANONYMOUS: String = "Anonymous"
const val PUBLIC: String = "Public"

@Serializable
enum class TargetType { GROUP, INDIVIDUAL }

@Serializable
enum class UpdateType { ADD, REMOVE, UPDATE }

@Serializable
data class Group(val organisation: String, val name: String) {
    constructor() : this("", "")

    var id: String = "$organisation-$name"

    override fun toString(): String {
        return id
    }
}

@Serializable
data class Target(val type: TargetType, val individualName: String? = null) {
    constructor() : this(TargetType.GROUP)
}

@Serializable
data class ChatItem(
    val userName: String = ANONYMOUS,
    val desc: String = "What What?",
    val priority: Int = 0,
    val date: Double = 0.0,
    val group: Group = Group(PUBLIC, ANONYMOUS),
    val target: Target = Target(TargetType.GROUP),
) {
    constructor() : this(userName = ANONYMOUS)

    var id: String = """_${userName.trim()}-${date.toLong()}"""
    val hash: String = desc.hashCode().toString()

    companion object {
        val endpoints = Endpoints("/chatList", "/chatListFromDate", "/lastItem")
    }

    fun urlEncodedId(): String = java.net.URLEncoder.encode(id, "utf-8")

    override fun toString(): String {
        return "$userName says: $desc (hash=$hash)"
    }

    data class Endpoints(val all: String, val fromDate: String, val last: String)
}

@Serializable
data class MostRecentData(val lastItem: ChatItem, val lastAlert: ChatItem? = null)

@Serializable
@IgnoreExtraProperties
data class UserLocation(
    val userName: String,
    val group: Group,
    val lon: Double,
    val lat: Double,
    val time: Long,
    val speed: Double,
    val altitude: Double,
    val accuracy: Double,
) : java.io.Serializable {
    constructor () : this("", userGroup, 0.0, 0.0, 0, 0.0, 0.0, 0.0)
    constructor (l: Location) : this(getDisplayName(),
        userGroup,
        l.longitude,
        l.latitude,
        l.time,
        l.speed.toDouble(),
        l.altitude,
        l.accuracy.toDouble())

    companion object {
        val endpoints = Endpoints("/userLocation", "/userLocation", "/allUserLocations")
    }

    data class Endpoints(val postLocation: String, val lastLocation: String, val allUserLocations: String)

    override fun hashCode(): Int {
        return userName.hashCode() + group.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return other?.let { return userName == (it as UserLocation).userName && group.id == it.group.id } ?: false
    }

    override fun toString(): String {
        return "${userName.possessiveForm()} Location Details:\ngroup:$group\nLongitude=$lon,\nLatitude=$lat,\nTime=${Date(time)},\nSpeed=${speed * 3.6}km/h,\nAltitude=${altitude}m,\nAccuracy=${accuracy}m"
    }
}

@Serializable
data class BluetoothItem(val name: String, val present: Boolean = false, val date: Long = Date().time) {
    override fun hashCode(): Int {
        return name.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return other?.let { return name == (it as BluetoothItem).name } ?: false
    }
}

fun String.possessiveForm(): String {
    return if (this.endsWith('s', true)) "${this}'" else "${this}'s"
}