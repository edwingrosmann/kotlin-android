package com.octopus

//import com.google.gson.reflect.TypeToken
import android.content.Intent
import android.database.DataSetObserver
import android.os.Bundle
import android.view.KeyEvent.*
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.octopus.bluetooth.BluetoothScanner
import com.octopus.bluetooth.BluetoothTimeServer
import com.octopus.chat.ChatListAdapter
import com.octopus.location.LocationViewer
import com.octopus.models.ChatItem
import com.octopus.models.UpdateType
import com.octopus.models.UserLocation
import com.octopus.qr.QrCodeScanner
import com.octopus.settings.Settings
import com.octopus.settings.getDisplayName
import com.octopus.settings.getShowLastItem
import com.octopus.util.hideKeyboard
import com.octopus.util.rotateAnimation
import kotlinx.coroutines.*
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var scope: CoroutineScope
    private val listObserver = ListObserver()

    public override fun onCreate(savedInstanceState: Bundle?) {
        logDebug(this, "Created")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addNewItemActionListener()
        addMessagePanelAcknowledge()
        scanLastItem()
//        storeAllUserLocations()
//        delete<ChatItem>(chatItemsQuery())
        listenForNewChatItems { d, a -> updateChatItemsList(d, a) }
        listenForUpdatedUserLocations { d, a -> updateUserLocations(d, a) }
    }

    override fun onDestroy() {
        logDebug(this, "Destroyed")
        super.onDestroy()
        scope.cancel()
        locationTrackService.stopListening()
    }

    override fun onResume() {
        super.onResume()
        initUsers()
    }

    /**
     * There is an auto-update text-field: the 'lastItem'.
     * This function update this field every second.
     */
    private fun scanLastItem() {
        scope = MainScope()
        scope.launch(Dispatchers.Main) {
            delay(500)
            while (true) {
                if (message().text != lastItemScanner.getStatusText()) {
                    clickRefreshButton()
                }
                updateMessagePanel()
                delay(1000)
            }
        }
    }

    private fun addMessagePanelAcknowledge() {
        messagePanel().setOnLongClickListener {
            if (!lastItemScanner.alertAcknowledged) {
                lastItemScanner.alertAcknowledged = true
            }
            true
        }
    }

    /**
     * This function detects if the enter-button is pressed on the new-item-field.
     * If so, a new List-item is submitted to the API.
     */
    private fun addNewItemActionListener() {
        val newItem = newItem()
        newItem.setOnEditorActionListener { _, keyCode, event ->
            if (keyCode == KEYCODE_ENDCALL || keyCode == KEYCODE_CALL || (event?.action == ACTION_DOWN && event.keyCode == KEYCODE_ENTER)) {
                addNewItem(newItem)
                true
            } else {
                false
            }
        }
    }

    fun openSettings(view: View) = view.rotateAnimation { startActivity(Intent(this, Settings::class.java)) }

    fun openLocationViewer(view: View) = view.rotateAnimation(0.25f) { startActivity(Intent(this, LocationViewer::class.java)) }

    fun openQrCodeScanner(view: View) = view.rotateAnimation { startActivity(Intent(this, QrCodeScanner::class.java)) }

    fun openBluetoothScanner(view: View) = view.rotateAnimation { startActivity(Intent(this, BluetoothScanner::class.java)) }

    fun openBluetoothTimeServer(view: View) = view.rotateAnimation { startActivity(Intent(this, BluetoothTimeServer::class.java)) }

    fun refresh(view: View) =
        view.rotateAnimation { scope.launch(Dispatchers.Main) { updateChatItemsList(getNewChatItems(listedChatItems())) } }

    /**
     * The new item-text provided will be sent off as a ChatListItem to the API.
     * After that the EditText is cleared and the page data is refreshed by emulating a refresh-button-click
     */
    private fun addNewItem(newItem: EditText) {

        if (newItem.text.isNotBlank()) {
            scope.launch(Dispatchers.IO) {
                val desc = newItem.text.toString()

                //Store the new item in the remote database...
                addChatListItem(ChatItem(userName = getDisplayName(), desc = desc, date = Date().time.toDouble(), group = userGroup))

                //Refresh the list...
                clickRefreshButton()

                //The user will see an immediate screen-update..
                lastItemScanner.lastItem = desc
                updateStatusField()

                //clear the just-processed-text-field...
                resetNewItem()
            }
            hideKeyboard(newItem)
        }
    }

    private fun updateStatusField() {
        runOnUiThread { message().text = lastItemScanner.getStatusText() }
    }

    /**
     * Get a copy of the chatItems
     */
    private fun listedChatItems(): ArrayList<ChatItem>? {
        return if (listView().adapter != null) (listView().adapter as ChatListAdapter).items else null
    }

    private fun updateMessagePanel() {
        messagePanel().visibility =
            if (lastItemScanner.isTroubleInParadise() || !lastItemScanner.alertAcknowledged) View.VISIBLE else if (getShowLastItem()) View.VISIBLE else View.GONE

        messageLabel().text =
            if (lastItemScanner.isTroubleInParadise()) "" else if (!lastItemScanner.alertAcknowledged) getString(R.string.alert) else getString(
                R.string.last_item)

        updateStatusField()
    }

    /**
     * Updates the existing List-data with the new items.
     * When the replace-flag has been set, the existing data will be replaced with the list provided.
     * Otherwise the chatItems will be ADDED to the existing list.
     */
    fun updateChatItemsList(chatItems: ArrayList<ChatItem>?, action: UpdateType = UpdateType.ADD, replace: Boolean = false) {
        if (chatItems != null) {
            runOnUiThread {
                with(listView()) {
                    unregisterListObserver()
                    if (adapter == null || replace) {
                        setAdapter(chatItems)
                    } else {
                        logDebug(this, "Updating list with $chatItems")
                        setAdapter(updateCurrentList(chatItems, action))
                    }
                }
            }
        }
    }

    private fun updateUserLocations(d: ArrayList<UserLocation>, a: UpdateType) {
        when (a) {
            UpdateType.ADD -> allUserLocations.addAll(d)
            UpdateType.REMOVE -> allUserLocations.removeAll(d)
            UpdateType.UPDATE -> {
                allUserLocations.removeAll(d); allUserLocations.addAll(d)
            }
        }
        allUserLocations.remove(currentLocation)
    }

    private fun ListView.setAdapter(chatItems: ArrayList<ChatItem>) {
        adapter = ChatListAdapter(chatItems, this@MainActivity)
        adapter.registerDataSetObserver(listObserver)
    }

    private fun ListView.updateCurrentList(items: ArrayList<ChatItem>, action: UpdateType): ArrayList<ChatItem> {
        with((adapter as ChatListAdapter).items) {
            when (action) {
                UpdateType.ADD, UpdateType.UPDATE -> addAll(items)
                UpdateType.REMOVE -> removeAll(items)
            }
            return this
        }
    }

    //UI Actions...
    private fun clickRefreshButton() = runOnUiThread { refreshButton().performClick() }
    private fun resetNewItem() = runOnUiThread { newItem().text?.clear(); newItem().clearFocus() }

    //UI Widgets...
    private fun newItem(): EditText = findViewById(R.id.new_item_text)
    fun listView(): ListView = findViewById(R.id.listView)
    private fun refreshButton(): Button = findViewById(R.id.refresh)
    private fun message(): TextView = findViewById(R.id.message)
    private fun messageLabel(): TextView = findViewById(R.id.messageLbl)
    private fun messagePanel(): LinearLayout = findViewById(R.id.messagePanel)

    /**
     * removes the listObserver from the ListView-adapter.
     * This is done when the adapter is going to be replaced with a new one.
     */
    private fun ListView.unregisterListObserver() {
        adapter?.unregisterDataSetObserver(listObserver)
    }

    /**
     * This is GSON black magic :-)
     * Please see https://kotlinlang.org/docs/reference/inline-functions.html and
     * https://stackoverflow.com/questions/33381384/how-to-use-typetoken-generics-with-gson-in-kotlin
     */
    //    inline fun <reified T> genericType() = object : TypeToken<T>() {}.type

    /**
     * When An Item get clicked it will be deleted. After that it fires a ```DataSetChangedEvent```;
     * This event can  be picked up by this class; with the re-fetching of the chat as a result.
     */
    private inner class ListObserver : DataSetObserver() {

        override fun onChanged() {
            scope.launch(Dispatchers.IO) { updateChatItemsList(getChatList(), UpdateType.ADD, true) }
        }

        override fun onInvalidated() {
            onChanged()
        }
    }
}