buildscript {
    repositories {
        gradlePluginPortal()
        jcenter()
        google()
        mavenCentral()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.0")
        classpath("com.android.tools.build:gradle:4.0.2")
        classpath("com.google.gms:google-services:4.3.4")
    }
}

allprojects{
    repositories{
        google()
    }
}

group = "com.octopus"
version = "1.0-SNAPSHOT"

repositories {
    gradlePluginPortal()
    jcenter()
    google()
    mavenCentral()
}
